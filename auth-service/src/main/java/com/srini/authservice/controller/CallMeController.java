package com.srini.authservice.controller;

import com.srini.authservice.client.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController

public class CallMeController {

    @Autowired
    private UserClient userClient;

    @RequestMapping("callMe")
    public String callMe(){
        String testMe = userClient.testMe();
        return testMe;
    }

}

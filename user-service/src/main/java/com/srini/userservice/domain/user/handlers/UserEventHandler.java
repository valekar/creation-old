package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.events.UserCreatedEvent;
import com.srini.userservice.domain.user.events.UserDeletedEvent;
import com.srini.userservice.domain.user.events.UserUpdatedEvent;
import com.srini.userservice.domain.user.model.User;
import com.srini.userservice.domain.user.repository.UserRepository;
import org.axonframework.eventhandling.EventHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.logging.Logger;

@Transactional
@Service
public class UserEventHandler extends BaseHandler{

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private PasswordEncoder passwordEncoder;

    private final static Logger LOGGER = Logger.getLogger(UserEventHandler.class.getName());

    @Autowired
    public UserEventHandler(UserRepository userRepository, PasswordEncoder encoder) {
        this.modelMapper = new ModelMapper();
        this.userRepository = userRepository;
        this.passwordEncoder = encoder;
    }

    @EventHandler
    public void handle(UserCreatedEvent userCreatedEvent) {
        LOGGER.info("User Created Event " + userCreatedEvent);
        String id = userCreatedEvent.getId();
        User user = modelMapper.map(userCreatedEvent.getUserData(), User.class);
        user.setPassword(passwordEncoder.encode(userCreatedEvent.getUserData().getPassword()));
        user.setId(id);
        user.setActive(true);

        User loggedInUser = userRepository.findByUsername(userCreatedEvent.getLoggedInUsername());
        if(loggedInUser !=null){
            user.setCreated_by(loggedInUser.getUsername());
            user.setCreated_user_id(loggedInUser.getId());
            user.setCreated_at(new Timestamp(System.currentTimeMillis()));
        }
        userRepository.save(user);
    }

    @EventHandler
    void handle(UserUpdatedEvent userUpdatedEvent) {
        LOGGER.info("User Updated Event: " + userUpdatedEvent);
        //ModelMapper modelMapper = new ModelMapper();
        User user = userRepository.findById(userUpdatedEvent.getId());
        if(user!=null) {
            User saveUser = modelMapper.map(userUpdatedEvent.getUserData(), User.class);
            saveUser.setUserId(user.getUserId());
            saveUser.setCreated_user_id(user.getCreated_user_id());
            saveUser.setCreated_by(user.getCreated_by());
            saveUser.setCreated_at(user.getCreated_at());
            saveUser.setActive(true);
            User loggedInUser = userRepository.findByUsername(userUpdatedEvent.getLoggedInUsername());
            if (loggedInUser != null) {
                saveUser = setAudits(loggedInUser,saveUser);
            }

            userRepository.save(saveUser);
        }
    }


    @EventHandler
    void handler(UserDeletedEvent userDeletedEvent){
        LOGGER.info("User Deleted Event: "+ userDeletedEvent);
        User user = userRepository.findById(userDeletedEvent.getId());
        if(user !=null) {
            user.setActive(false);
            User loggedInUser = userRepository.findByUsername(userDeletedEvent.getLoggedInUsername());
            if (loggedInUser != null) {
                user = setAudits(loggedInUser,user);
            }
            userRepository.save(user);
            //userRepository.delete(user);
        }
    }




}

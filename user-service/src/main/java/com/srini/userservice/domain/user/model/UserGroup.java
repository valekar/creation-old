package com.srini.userservice.domain.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "user_groups")
public class UserGroup {


    @Column(name ="user_group_id")
    @Id
    @javax.persistence.Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long user_group_id;

    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;


    @Embedded
    private Privilege privileges;

    @Column(name = "is_active")
    private boolean active;

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_user_id")
    private String created_user_id;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_user_id")
    private String updated_user_id;

}

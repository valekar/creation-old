package com.srini.userservice.domain.user.repository;

import com.srini.userservice.domain.user.model.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

    @Query("FROM UserGroup u where u.id = :id and active = true")
    UserGroup findUserGroupById(@Param("id") String id);

    @Query("FROM UserGroup u where u.name = :name and active = true")
    UserGroup findUserGroupByName(@Param("name") String name);
}

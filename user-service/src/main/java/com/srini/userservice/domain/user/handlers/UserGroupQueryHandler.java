package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.model.UserGroup;
import com.srini.userservice.domain.user.repository.UserGroupRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class UserGroupQueryHandler {

    private UserGroupRepository userGroupRepository;

    public UserGroupQueryHandler(UserGroupRepository userGroupRepository){
        this.userGroupRepository = userGroupRepository;
    }

    public UserGroup findUserGroupById(String id){
        return userGroupRepository.findUserGroupById(id);
    }
    public UserGroup findUserGroupByName(String name){
        return userGroupRepository.findUserGroupByName(name);
    }
}

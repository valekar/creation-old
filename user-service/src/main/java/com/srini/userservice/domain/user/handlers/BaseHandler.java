package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.model.User;
import com.srini.userservice.domain.user.model.UserGroup;

import java.sql.Timestamp;

public class BaseHandler {


    // USER
     User setAudits(User loggedInUser,User modelUser){
        modelUser.setUpdated_by(loggedInUser.getUsername());
        modelUser.setUpdated_user_id(loggedInUser.getId());
        modelUser.setUpdated_at(new Timestamp(System.currentTimeMillis()));

        return modelUser;
    }


    //USER GROUP
    UserGroup setAudits(User loggedInUser, UserGroup userGroup){
        userGroup.setUpdated_by(loggedInUser.getUsername());
        userGroup.setUpdated_user_id(loggedInUser.getId());
        userGroup.setUpdated_at(new Timestamp(System.currentTimeMillis()));
         return userGroup;
    }

}

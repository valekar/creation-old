package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.commands.UserCreateCmd;
import com.srini.userservice.domain.user.commands.UserDeleteCmd;
import com.srini.userservice.domain.user.commands.UserUpdateCmd;
import com.srini.userservice.domain.user.events.UserCreatedEvent;
import com.srini.userservice.domain.user.events.UserDeletedEvent;
import com.srini.userservice.domain.user.events.UserUpdatedEvent;
import com.srini.userservice.domain.user.model.User;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.modelmapper.ModelMapper;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;


@Aggregate
public class UserAggregate {

    @AggregateIdentifier
    private String id;
    private User user;
    private boolean isActive;
    private String loggedInUser;

    UserAggregate(){}

    @CommandHandler
    public UserAggregate(UserCreateCmd userCreateCmd){
        apply(new UserCreatedEvent(userCreateCmd.getId(),userCreateCmd.getUserData(),userCreateCmd.getLoggedInUsername()));
    }

    @CommandHandler
    public void on(UserUpdateCmd userUpdateCmd){
        apply(new UserUpdatedEvent(userUpdateCmd.getId(),userUpdateCmd.getUserData(),userUpdateCmd.getLoggedInUsername()));
    }

    @CommandHandler
    public void on(UserDeleteCmd userDeleteCmd){
        apply(new UserDeletedEvent(userDeleteCmd.getId(),userDeleteCmd.getUserData(),userDeleteCmd.getLoggedInUsername()));
    }


    @EventSourcingHandler
    public void on(UserCreatedEvent userCreatedEvent){
        id = userCreatedEvent.getId();
        ModelMapper mapper = new ModelMapper();
        user = mapper.map(userCreatedEvent.getUserData(), User.class);
        user.setId(id);
        isActive = true;
        loggedInUser = userCreatedEvent.getLoggedInUsername();
    }


    @EventSourcingHandler
    public void on(UserUpdatedEvent event){
        id = event.getId();
        ModelMapper modelMapper = new ModelMapper();
        user = modelMapper.map(event.getUserData(), User.class);
        loggedInUser  = event.getLoggedInUsername();
    }

    @EventSourcingHandler
    public void on(UserDeletedEvent event){
        id = event.getId();
        ModelMapper modelMapper = new ModelMapper();
        user = modelMapper.map(event.getUserData(), User.class);
        isActive = false;
        loggedInUser = event.getLoggedInUsername();
    }
}

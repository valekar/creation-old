
package com.srini.userservice.domain.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class UserDetails{

    @Column(name="age")
    private Long age;

    @Embedded
    private Address address;

    @Column(name = "sex")
    private String sex;

    @Column(name = "blood_group")
    private String blood_group;

}



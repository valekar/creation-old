package com.srini.userservice.domain.user.facade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRequest {
    private UserData userData;
    private String loggedInUsername;
}

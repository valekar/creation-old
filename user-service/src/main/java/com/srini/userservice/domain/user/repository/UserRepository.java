package com.srini.userservice.domain.user.repository;

import com.srini.userservice.domain.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    //@Query("FROM User u where u.username = :username")
    @Query("FROM User u where u.username = :username and active = true")
    User findByUsername(@Param("username") String username);

    @Query("FROM User u where u.username = :username and u.email = :email and active = true")
    User findByEmailAndUsername(@Param("email") String email, @Param("username") String username);

    @Query("FROM User u where u.email = :email and active = true")
    User findByEmail(@Param("email") String email);

    @Query("FROM User u where u.id = :id and active = true")
    User findById(@Param("id") String id);
}
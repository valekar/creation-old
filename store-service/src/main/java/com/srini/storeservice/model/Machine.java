package com.srini.storeservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "machines")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Machine {

    @javax.persistence.Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "machine_id", nullable = false, updatable = false)
    private Long machineId;

    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "heads")
    private Integer heads;

    @Column(name = "type")
    private String type;

    @Column(name = "stitch_capacity")
    private Long stitchCapacity;

    @Column(name = "is_active")
    private Boolean active;

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_user_id")
    private String created_user_id;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_user_id")
    private String updated_user_id;
}

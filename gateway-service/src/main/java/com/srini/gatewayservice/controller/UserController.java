package com.srini.gatewayservice.controller;

import com.srini.gatewayservice.client.UserClient;
import com.srini.gatewayservice.facade.userservice.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PreAuthorize("hasPermission('', 'admin', 'true')")
@RequestMapping("/user")
public class UserController {

    private UserClient userClient;

    public UserController(UserClient userClient){
        this.userClient = userClient;
    }

    @GetMapping("/{id}")
    public User getUserByUsername(@PathVariable("id") String username){
        return userClient.getUserByName(username);
    }
}

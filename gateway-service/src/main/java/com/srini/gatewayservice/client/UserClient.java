package com.srini.gatewayservice.client;

import com.srini.gatewayservice.facade.userservice.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Primary
@FeignClient(name="user-service",fallback = UserClientFallback.class)
public interface UserClient {

    //@RequestMapping(method = RequestMethod.PUT, value = "/statistics/{accountName}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("/user/{id}")
    User getUserByName(@PathVariable("id") String username);


}


@Component
class UserClientFallback implements UserClient{


    @Override
    public User getUserByName(String username)
    {
        User user = new User("fallback_user");
        return user ;
    }

}
